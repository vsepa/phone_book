// Телефонная книга
var phoneBook = {};

/**
 * @param {String} command
 * @returns {*} - результат зависит от команды
 */
module.exports = function (command) {

  var commandArr = command.split(' ');

  var commandName = commandArr[0];

  if (commandName === 'ADD') {
    var name = commandArr[1];
    var phones = commandArr[2].split(',');
    return addContact(name, phones);
  }
  
  if (commandName === 'REMOVE_PHONE') {
    var phone = commandArr[1];
    return removeContact(phone);
  }

  if (commandName === 'SHOW') {
    return show();
  }
};

function addContact(name, phones) {
  if (!phoneBook.hasOwnProperty(name)) {
    phoneBook[name] = [];
  } 

  phoneBook[name] = phoneBook[name].concat(phones);
}

function removeContact(phone) {
  var names = Object.keys(phoneBook);

  if (names.length === 0) {
    return false;
  }

  for (var i = 0; i < names.length; i++) {

    var name = names[i];
    var phones = phoneBook[name];
    var index = phones.indexOf(phone);
    if (index > -1) {
      removeContactPhoneAt(name, index);
      return true;
    }
  }

  return false;
}

function removeContactPhoneAt(name, index) {
  phoneBook[name].splice(index, 1);

  if(phoneBook[name].length === 0) {
    delete phoneBook[name];
  }
}

function show() {
  var arr = [];

  var names = Object.keys(phoneBook);

  for (var i = 0; i < names.length; i++) {
    var name = names[i];

    if (phoneBook[name].length !== 0) {
      var contactStr = name + ': ' + phoneBook[name].join(', ');
      arr.push(contactStr);
    }
  }

  return arr.sort();
}

















